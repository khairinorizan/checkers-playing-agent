package edu.iastate.cs472.proj1;

/**
 * @author Muhammad Khairi Norizan
 */
public class AlphaBetaSearch {
    private CheckersData board;

    // An instance of this class will be created in the Checkers.Board
    // It would be better to keep the default constructor.

    public void setCheckersData(CheckersData board) {
        this.board = board;
    }

    // Todo: You can implement your helper methods here
    public CheckersData simulateNewBoard(CheckersMove move){
        //Shallow copy of current board
        CheckersData newBoard = new CheckersData(this.board);

        newBoard.makeMove(move);

        return newBoard;
    }

    /**
     * Heuristic/Evaluation Function for the board
     * @param currentBoard
     * @return the heuristic value
     */
    public Integer heuristicFunction(CheckersData currentBoard){
        int retVal = ((currentBoard.getNumberOfPieces(CheckersData.BLACK) + (5 * currentBoard.getNumberOfPieces(CheckersData.BLACK_KING))) -
                (currentBoard.getNumberOfPieces(CheckersData.RED)) + (5 * currentBoard.getNumberOfPieces(CheckersData.RED_KING)));

        return retVal;
    }

    /**
     *
     * @param boardConfiguration
     * @param depth
     * @param max_min
     * @param alpha
     * @param beta
     * @return
     */
    public Integer miniMax(CheckersData boardConfiguration, int depth, boolean max_min, int alpha, int beta){
        // When reached the leaf node
        if(depth == 0 || (boardConfiguration.getLegalMoves(CheckersData.BLACK) == null ||
                        boardConfiguration.getLegalMoves(CheckersData.RED) == null)){
            return this.heuristicFunction(boardConfiguration);
        }

        int retVal = 0;

        //true is max
        if(max_min){
            CheckersMove bestMove = null;

            CheckersMove[] allLegalMoves = boardConfiguration.getLegalMoves(CheckersData.BLACK);

            for(CheckersMove checkersMove: allLegalMoves){
                if(alpha >= beta){
                    continue;
                }else{
                    CheckersData cloneBoard = new CheckersData(boardConfiguration);
                    cloneBoard.makeMove(checkersMove);

                    int evaluation = miniMax(cloneBoard, depth-1, false, alpha, beta);

                    if(evaluation > alpha){
                        alpha = evaluation;
                        bestMove = checkersMove;
                    }

                }

            }
            retVal = alpha;

        //false is min
        }else{
            CheckersMove bestMove = null;

            CheckersMove[] allLegalMoves = boardConfiguration.getLegalMoves(CheckersData.RED);

            for(CheckersMove checkersMove: allLegalMoves){
                if(alpha >= beta){
                    continue;
                }else{
                    CheckersData cloneBoard = new CheckersData(boardConfiguration);
                    cloneBoard.makeMove(checkersMove);

                    int evaluation = miniMax(cloneBoard, depth-1, true, alpha, beta);

                    if(evaluation < beta){
                        beta = evaluation;
                        bestMove = checkersMove;
                    }
                }

            }
            retVal = beta;

        }

        return retVal;
    }

    /**
     *  You need to implement the Alpha-Beta pruning algorithm here to
     * find the best move at current stage.
     * The input parameter legalMoves contains all the possible moves.
     * It contains four integers:  fromRow, fromCol, toRow, toCol
     * which represents a move from (fromRow, fromCol) to (toRow, toCol).
     * It also provides a utility method `isJump` to see whether this
     * move is a jump or a simple move.
     *
     * @param legalMoves All the legal moves for the agent at current step.
     */
    public CheckersMove makeMove(CheckersMove[] legalMoves) {
        // The checker board state can be obtained from this.board,
        // which is a int 2D array. The numbers in the `board` are
        // defined as
        // 0 - empty square,
        // 1 - red man
        // 2 - red king
        // 3 - black man
        // 4 - black king
        System.out.println(board);
        System.out.println();

        CheckersMove bestMove = null;

        int alpha = Integer.MIN_VALUE;
        int beta = Integer.MAX_VALUE;

        // Todo: return the move for the current state
        for(CheckersMove checkersMove: legalMoves){
            if(alpha >= beta){
                continue;
            }else{
                //Clone current board
                CheckersData cloneBoard = new CheckersData(this.board);
                //Make move with the current checkersMove on cloneBoard
                cloneBoard.makeMove(checkersMove);

                System.out.println(cloneBoard);
                System.out.println();
                /**
                 * Minimax always start with finding minimum value of the branch since all the
                 * legalMoves are on the second level of the tree
                 * */
                int evaluation = miniMax(cloneBoard, 6, false, alpha, beta);

                if(evaluation > alpha){
                    alpha = evaluation;
                    bestMove = checkersMove;
                }

            }

        }

        // Here, we simply return the first legal move for demonstration.
        return bestMove;
    }
}
