package edu.iastate.cs472.proj1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * An object of this class holds data about a game of checkers.
 * It knows what kind of piece is on each square of the checkerboard.
 * Note that RED moves "up" the board (i.e. row number decreases)
 * while BLACK moves "down" the board (i.e. row number increases).
 * Methods are provided to return lists of available legal moves.
 */

/**
 * @author Muhammad Khairi Norizan
 */
public class CheckersData {

  /*  The following constants represent the possible contents of a square
      on the board.  The constants RED and BLACK also represent players
      in the game. */

    static final int
            EMPTY = 0,
            RED = 1,
            RED_KING = 2,
            BLACK = 3,
            BLACK_KING = 4;


    int[][] board;  // board[r][c] is the contents of row r, column c.


    /**
     * Constructor.  Create the board and set it up for a new game.
     */
    CheckersData() {
        board = new int[8][8];
        setUpGame();
    }

    CheckersData(CheckersData oldBoard){
        board = new int[8][8];
        //deep copy board
        for(int row=0; row < 8; row++){
            for(int col=0; col < 8; col++){
                board[row][col] = oldBoard.pieceAt(row,col);
            }
        }
    }

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_YELLOW = "\u001B[33m";

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < board.length; i++) {
            int[] row = board[i];
            sb.append(8 - i).append(" ");
            for (int n : row) {
                if (n == 0) {
                    sb.append(" ");
                } else if (n == 1) {
                    sb.append(ANSI_RED + "R" + ANSI_RESET);
                } else if (n == 2) {
                    sb.append(ANSI_RED + "K" + ANSI_RESET);
                } else if (n == 3) {
                    sb.append(ANSI_YELLOW + "B" + ANSI_RESET);
                } else if (n == 4) {
                    sb.append(ANSI_YELLOW + "K" + ANSI_RESET);
                }
                sb.append(" ");
            }
            sb.append(System.lineSeparator());
        }
        sb.append("  a b c d e f g h");

        return sb.toString();
    }

    /**
     * Set up the board with checkers in position for the beginning
     * of a game.  Note that checkers can only be found in squares
     * that satisfy  row % 2 == col % 2.  At the start of the game,
     * all such squares in the first three rows contain black squares
     * and all such squares in the last three rows contain red squares.
     */
    /**
     * @author Muhammad Khairi Norizan
     */
    void setUpGame() {
        // Todo: setup the board with pieces BLACK, RED, and EMPTY

        /* Fill first 3 row */
        for(int i=0; i < 3; i++){
            for(int j=0; j < 8; j++){
                if((i % 2) == (j % 2)){
                    board[i][j] = BLACK;
                }else{
                    board[i][j] = EMPTY;
                }
            }
        }

        /* Fill middle board with EMPTY */
        for(int i=3; i<5; i++){
            for(int j=0; j<8; j++){
                board[i][j] = EMPTY;
            }
        }

        /* Fill the last 3 row */
        for(int i=5; i<8; i++){
            for(int j=0; j<8; j++){
                if((i % 2) == (j % 2)){
                    board[i][j] = RED;
                }else{
                    board[i][j] = EMPTY;
                }
            }
        }
    }


    /**
     * Return the contents of the square in the specified row and column.
     */
    int pieceAt(int row, int col) {
        return board[row][col];
    }


    /**
     * Make the specified move.  It is assumed that move
     * is non-null and that the move it represents is legal.
     * @return  true if the piece becomes a king, otherwise false
     */
    boolean makeMove(CheckersMove move) {
        return makeMove(move.fromRow, move.fromCol, move.toRow, move.toCol);
    }


    /**
     * Make the move from (fromRow,fromCol) to (toRow,toCol).  It is
     * assumed that this move is legal.  If the move is a jump, the
     * jumped piece is removed from the board.  If a piece moves to
     * the last row on the opponent's side of the board, the
     * piece becomes a king.
     *
     * @param fromRow row index of the from square
     * @param fromCol column index of the from square
     * @param toRow   row index of the to square
     * @param toCol   column index of the to square
     * @return        true if the piece becomes a king, otherwise false
     */
    /**
     * @author Muhammad Khairi Norizan
     */
    boolean makeMove(int fromRow, int fromCol, int toRow, int toCol) {
        // Todo: update the board for the given move.
        // You need to take care of the following situations:
        // 1. move the piece from (fromRow,fromCol) to (toRow,toCol)
        // 2. if this move is a jump, remove the captured piece
        // 3. if the piece moves into the kings row on the opponent's side of the board, crowned it as a king

        /* get the piece at position */
        int pieceAtPosition = pieceAt(fromRow, fromCol);

        CheckersMove checkersMove = new CheckersMove(fromRow, fromCol, toRow, toCol);

        if(pieceAtPosition == RED){
            if(checkersMove.isJump()){
                if((fromRow-2 >= 0 && fromCol+2 < 8) && (fromRow-1 >= 0 && fromCol+1 < 8)
                        && (pieceAt(toRow, toCol) == EMPTY)){
                    /* Remove the capture piece */
                    if(toRow == (fromRow - 2) && toCol == (fromCol + 2)){
                        board[fromRow - 1][fromCol + 1] = EMPTY;
                    }
                }

                if((fromRow-2 >= 0 && fromCol-2 >= 0) && (fromRow-1 >= 0 && fromCol-1 >= 0)
                        && (pieceAt(toRow, toCol) == EMPTY)){
                    /* Remove the capture piece */
                    if(toRow == (fromRow - 2) && toCol == (fromCol - 2)){
                        board[fromRow - 1][fromCol - 1] = EMPTY;
                    }
                }
            }
            if(toRow == 0){
                board[fromRow][fromCol] = EMPTY;
                board[toRow][toCol] = RED_KING;

                return true;
            }else{
                board[fromRow][fromCol] = EMPTY;
                board[toRow][toCol] = RED;

                return false;
            }

        }else if(pieceAtPosition == BLACK){
            if(checkersMove.isJump()){
                if((fromRow+2 < 8 && fromCol+2 < 8) && (fromRow+1 < 8 && fromCol+1 < 8)
                        && (pieceAt(toRow, toCol) == EMPTY)){
                    /* Remove the capture piece */
                    if(toRow == (fromRow + 2) && toCol == (fromCol + 2)){
                        board[fromRow + 1][fromCol + 1] = EMPTY;
                    }
                }
                if((fromRow+2 < 8 && fromCol-2 >= 0) && (fromRow+1 < 8 && fromCol-1 >= 0)
                        && (pieceAt(toRow, toCol) == EMPTY)){
                    /* Remove the capture piece */
                    if(toRow == (fromRow + 2) && toCol == (fromCol - 2)){
                        board[fromRow + 1][fromCol - 1] = EMPTY;
                    }
                }

            }
            if(toRow == 7){
                board[fromRow][fromCol] = EMPTY;
                board[toRow][toCol] = BLACK_KING;

                return true;
            }else{
                board[fromRow][fromCol] = EMPTY;
                board[toRow][toCol] = BLACK;

                return false;
            }

        }else if(pieceAtPosition == RED_KING || pieceAtPosition == BLACK_KING){
            if(checkersMove.isJump()){
                if((fromRow+2 < 8 && fromCol+2 < 8) && (fromRow+1 < 8 && fromCol+1 < 8)
                        && (pieceAt(toRow, toCol) == EMPTY)){
                    /* Remove the capture piece */
                    if(toRow == (fromRow + 2) && toCol == (fromCol + 2)){
                        board[fromRow + 1][fromCol + 1] = EMPTY;
                    }
                }

                if((fromRow+2 < 8 && fromCol-2 >= 0) && (fromRow+1 < 8 && fromCol-1 >= 0)
                        && (pieceAt(toRow, toCol) == EMPTY)){
                    /* Remove the capture piece */
                    if(toRow == (fromRow + 2) && toCol == (fromCol - 2)){
                        board[fromRow + 1][fromCol - 1] = EMPTY;
                    }
                }

                if((fromRow-2 >= 0 && fromCol+2 < 8) && (fromRow-1 >= 0 && fromCol+1 < 8)
                        && (pieceAt(toRow, toCol) == EMPTY)){
                    /* Remove the capture piece */
                    if(toRow == (fromRow - 2) && toCol == (fromCol + 2)){
                        board[fromRow - 1][fromCol + 1] = EMPTY;
                    }
                }

                if((fromRow-2 >= 0 && fromCol-2 >= 0) && (fromRow-1 >= 0 && fromCol-1 >= 0)
                        && (pieceAt(toRow, toCol) == EMPTY)){
                    /* Remove the capture piece */
                    if(toRow == (fromRow - 2) && toCol == (fromCol - 2)){
                        board[fromRow - 1][fromCol - 1] = EMPTY;
                    }
                }

            }

            /* Make board[toRow][toCol] = RED_KING */
            if(pieceAtPosition == RED_KING){
                board[fromRow][fromCol] = EMPTY;
                board[toRow][toCol] = RED_KING;
            }
            /* Make board[toRow][toCol] = BLACK_KING */
            if(pieceAtPosition == BLACK_KING){
                board[fromRow][fromCol] = EMPTY;
                board[toRow][toCol] = BLACK_KING;
            }

            return false;

        }

        return false;
    }

    /**
     * Return an array containing all the legal CheckersMoves
     * for the specified player on the current board.  If the player
     * has no legal moves, null is returned.  The value of player
     * should be one of the constants RED or BLACK; if not, null
     * is returned.  If the returned value is non-null, it consists
     * entirely of jump moves or entirely of regular moves, since
     * if the player can jump, only jumps are legal moves.
     *
     * @param player color of the player, RED or BLACK
     */
    /**
     * @author Muhammad Khairi Norizan
     */
    CheckersMove[] getLegalMoves(int player) {
        // Todo: Implement your getLegalMoves here.
        List<CheckersMove> checkersMoveList = new ArrayList<CheckersMove>();

        /* Check if the player is not BLACK AND RED */
        if(player != RED && player != BLACK){
            return null;
        }

        for(int row=0; row < 8; row++){
            for(int col=0; col < 8; col++){
                if(pieceAt(row, col) == player || pieceAt(row, col) == player + 1){
                    //Get all legal jump of current piece
                    CheckersMove[] jumpMoves = getLegalJumpsFrom(pieceAt(row, col), row, col);

                    if((pieceAt(row, col) == RED) &&  (pieceAt(row, col) == player)){
                        // Move Right Forward
                        if(row-1 >= 0 && col+1 < 8){
                            if(board[row-1][col+1] == EMPTY){
                                CheckersMove checkersMove = new CheckersMove(row, col, row-1, col+1);
                                checkersMoveList.add(checkersMove);
                            }
                        }
                        // Move Left Forward
                        if(row-1 >= 0 && col-1 >= 0){
                            if(board[row-1][col-1] == EMPTY){
                                CheckersMove checkersMove = new CheckersMove(row, col, row-1, col-1);
                                checkersMoveList.add(checkersMove);
                            }
                        }
                    }

                    if(pieceAt(row, col) == BLACK &&  (pieceAt(row, col) == player)){
                        // Move Left Forward
                        if(row+1 < 8 && col+1 < 8){
                            if(board[row+1][col+1] == EMPTY){
                                CheckersMove checkersMove = new CheckersMove(row, col, row+1, col+1);
                                checkersMoveList.add(checkersMove);
                            }
                        }

                        //Move Right Forward
                        if(row+1 < 8 && col-1 >= 0){
                            if(board[row+1][col-1] == EMPTY){
                                CheckersMove checkersMove = new CheckersMove(row, col, row+1, col-1);
                                checkersMoveList.add(checkersMove);
                            }
                        }
                    }

                    if((pieceAt(row, col) == BLACK_KING || pieceAt(row, col) == RED_KING)  &&  (pieceAt(row, col) == player + 1)){
                        /*
                         *
                         * Red_King = Move Right Forward
                         * Black_King = Move left Backward
                         *
                         * */
                        if(row-1 >= 0 && col+1 < 8){
                            if(board[row-1][col+1] == EMPTY){
                                CheckersMove checkersMove = new CheckersMove(row, col, row-1, col+1);
                                checkersMoveList.add(checkersMove);
                            }
                        }

                        /*
                         *
                         * Red_King = Move left Forward
                         * Black_King = Move right Backward
                         *
                         * */
                        if(row-1 >= 0 && col-1 >= 0){
                            if(board[row-1][col-1] == EMPTY){
                                CheckersMove checkersMove = new CheckersMove(row, col, row-1, col-1);
                                checkersMoveList.add(checkersMove);
                            }
                        }

                        /*
                         *
                         * Red_King = Move Right Backward
                         * Black_King = Move Left Forward
                         *
                         * */
                        if(row+1 < 8 && col+1 < 8){
                            if(board[row+1][col+1] == EMPTY){
                                CheckersMove checkersMove = new CheckersMove(row, col, row+1, col+1);
                                checkersMoveList.add(checkersMove);
                            }
                        }

                        /*
                         *
                         * Red_King = Move left Backward
                         * Black_King = Move Right Forward
                         *
                         * */
                        if(row+1 < 8 && col-1 >= 0){
                            if(board[row+1][col-1] == EMPTY){
                                CheckersMove checkersMove = new CheckersMove(row, col, row+1, col-1);
                                checkersMoveList.add(checkersMove);
                            }
                        }
                    }

                    if(jumpMoves != null){
                        for(int i=0; i < jumpMoves.length; i++){
                            checkersMoveList.add(jumpMoves[i]);
                        }
                    }
                }
            }
        }

        CheckersMove[] checkersMovesArr = new CheckersMove[checkersMoveList.size()];

        for(int i=0; i < checkersMoveList.size(); i++){
            checkersMovesArr[i] = checkersMoveList.get(i);
        }

        return checkersMovesArr;
    }

    /**
     * Return a list of the legal jumps that the specified player can
     * make starting from the specified row and column.  If no such
     * jumps are possible, null is returned.  The logic is similar
     * to the logic of the getLegalMoves() method.
     *
     * @param player The player of the current jump, either RED or BLACK.
     * @param row    row index of the start square.
     * @param col    col index of the start square.
     */
    /**
     * @author Muhammad Khairi Norizan
     */
    CheckersMove[] getLegalJumpsFrom(int player, int row, int col) {
        // Todo: Implement your getLegalJumpsFrom here.
        List<CheckersMove> checkersMoveList = new ArrayList<CheckersMove>();

        if(player == RED){
            //Right Jump
            if((row-2 >= 0 && col+2 < 8) && (row-1 >= 0 && col+1 < 8)){
                if((board[row-1][col+1] == BLACK || board[row-1][col+1] == BLACK_KING) &&
                        (pieceAt(row-2, col+2) == EMPTY)){
                    CheckersMove checkersMove = new CheckersMove(row, col, row-2, col+2);

                    checkersMoveList.add(checkersMove);
                }
            }

            //Left Jump
            if((row-2 >= 0 && col-2 >= 0) && (row-1 >= 0 && col-1 >= 0)){
                if((board[row-1][col-1] == BLACK || board[row-1][col-1] == BLACK_KING) && 
                        (pieceAt(row-2,col-2) == EMPTY)){
                    CheckersMove checkersMove = new CheckersMove(row, col, row-2, col-2);

                    checkersMoveList.add(checkersMove);
                }
            }

        }else if(player == BLACK){
            //Right Jump
            if((row + 2 < 8 && col + 2 < 8) && (row + 1 < 8 && col + 1 < 8)){
                if((board[row+1][col+1] == RED || board[row+1][col+1] == RED_KING) &&
                        (pieceAt(row+2, col+2) == EMPTY)){
                    CheckersMove checkersMove = new CheckersMove(row, col, row+2, col+2);
    
                    checkersMoveList.add(checkersMove);
                }
            }
    
            //Left Jump
            if((row+2 < 8 && col-2 >= 0) && (row+1 < 8 && col-1 >= 0)){
                if((board[row+1][col-1] == RED || board[row+1][col-1] == RED_KING) &&
                        (pieceAt(row+2, col-2) == EMPTY)){
                    CheckersMove checkersMove = new CheckersMove(row, col, row+2, col-2);
    
                    checkersMoveList.add(checkersMove);
                }
            }
    
        }else if(player == RED_KING){
            // Forward Right Jump
            if((row-2 >= 0 && col+2 < 8) && (row-1 >= 0 && col+1 < 8)){
                if((board[row-1][col+1] == BLACK || board[row-1][col+1] == BLACK_KING) &&
                        (pieceAt(row-2,col+2) == EMPTY)){
                    CheckersMove checkersMove = new CheckersMove(row, col, row-2, col+2);
    
                    checkersMoveList.add(checkersMove);
                }
            }
    
            // Forward Left Jump
            if((row-2 >= 0 && col-2 >= 0) && (row-1 >= 0 && col-1 >= 0)){
                if((board[row-1][col-1] == BLACK || board[row-1][col-1] == BLACK_KING) &&
                        (pieceAt(row-2, col-2) == EMPTY)){
                    CheckersMove checkersMove = new CheckersMove(row, col, row-2, col-2);
    
                    checkersMoveList.add(checkersMove);
                }
            }
    
            // Backward Right Jump
            if((row + 2 < 8 && col + 2 < 8) && (row + 1 < 8 && col + 1 < 8)){
                if((board[row+1][col+1] == BLACK || board[row+1][col+1] == BLACK_KING) &&
                        (pieceAt(row+2, col+2) == EMPTY)){
                    CheckersMove checkersMove = new CheckersMove(row, col, row+2, col+2);
    
                    checkersMoveList.add(checkersMove);
                }
            }
    
            // Backward Left Jump
            if((row+2 < 8 && col-2 >= 0) && (row+1 < 8 && col-1 >= 0)){
                if((board[row+1][col-1] == BLACK || board[row+1][col-1] == BLACK_KING) &&
                            (pieceAt(row+2, col-2) == EMPTY)){
                    CheckersMove checkersMove = new CheckersMove(row, col, row+2, col-2);

                    checkersMoveList.add(checkersMove);
                }
            }
    
        }else if(player == BLACK_KING){
            // Forward Right Jump
            if((row-2 >= 0 && col+2 < 8) && (row-1 >= 0 && col+1 < 8)){
                if((board[row-1][col+1] == RED || board[row-1][col+1] == RED_KING) &&
                        (pieceAt(row-2,col+2) == EMPTY)){
                    CheckersMove checkersMove = new CheckersMove(row, col, row-2, col+2);

                    checkersMoveList.add(checkersMove);
                }
            }

            // Forward Left Jump
            if((row-2 >= 0 && col-2 >= 0) && (row-1 >= 0 && col-1 >= 0)){
                if((board[row-1][col-1] == RED || board[row-1][col-1] == RED_KING) &&
                        (pieceAt(row-2, col-2) == EMPTY)){
                    CheckersMove checkersMove = new CheckersMove(row, col, row-2, col-2);

                    checkersMoveList.add(checkersMove);
                }
            }

            // Backward Right Jump
            if((row + 2 < 8 && col + 2 < 8) && (row + 1 < 8 && col + 1 < 8)){
                if((board[row+1][col+1] == RED || board[row+1][col+1] == RED_KING) &&
                        (pieceAt(row+2, col+2) == EMPTY)){
                    CheckersMove checkersMove = new CheckersMove(row, col, row+2, col+2);

                    checkersMoveList.add(checkersMove);
                }
            }

            // Backward Left Jump
            if((row+2 < 8 && col-2 >= 0) && (row+1 < 8 && col-1 >= 0)){
                if((board[row+1][col-1] == RED || board[row+1][col-1] == RED_KING) &&
                        (pieceAt(row+2, col-2) == EMPTY)){
                    CheckersMove checkersMove = new CheckersMove(row, col, row+2, col-2);

                    checkersMoveList.add(checkersMove);
                }
            }
        }

        if(checkersMoveList.isEmpty()){
            return null;
        }

        CheckersMove[] checkersMovesArr = new CheckersMove[checkersMoveList.size()];

        for(int i=0; i < checkersMoveList.size(); i++){
            checkersMovesArr[i] = checkersMoveList.get(i);
        }

        return checkersMovesArr;
    }

    /**
     * Get the number of pieces left of the board for the specific player
     * @param player
     * @return number of player left on the board
     */
    public Integer getNumberOfPieces(int player){

        int pieceCount = 0;

        for(int row=0; row < 8; row++){
            for(int col=0; col < 8; col++){
                if(pieceAt(row, col) == player || pieceAt(row, col) == player + 1){
                    pieceCount++;
                }
            }
        }

        return pieceCount;
    }

}